using Sirenix.OdinInspector.Editor.Validation;
using UnityEngine;
using Valhalla.AddressableUtilities.AssetReferences;
using Valhalla.AddressableUtilities.Editor.Validators;


[assembly: RegisterValidator(typeof(AddressableAssetAttributeValidator<>))]

namespace Valhalla.AddressableUtilities.Editor.Validators
{
	public sealed class AddressableAssetAttributeValidator<TAsset> : AttributeValidator<AddressableAssetAttribute, TAsset>
		where TAsset : ScriptableObject
	{
		private static string _lastFileNameGeneric = "";
		
		
		private class FixArgs
		{
			public string Address;
			
			public FixArgs()
			{
				Address = _lastFileNameGeneric;
			}
		}
		
		protected override void Validate(ValidationResult result)
		{
			if (!Value.IsAddressable())
			{
				_lastFileNameGeneric = ConvertAssetNameToSuggestedAddress();
				
				result
					.AddError($"Asset <{typeof(TAsset).Name}> must be an addressable!")
					.WithFix((FixArgs args) => MakeAssetAddressable(args.Address));
			}
		}


		private string ConvertAssetNameToSuggestedAddress()
			=> Attribute.NameDecomposeRegex.Replace(Value.name, Attribute.NameToAddressReplaceScheme);


		private void MakeAssetAddressable(string initialAddress)
		{
			Value.MakeAnAddressable(initialAddress);
		}
	}
}
