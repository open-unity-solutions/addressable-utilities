using System;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
using UnityEngine;
using Object = UnityEngine.Object;


namespace Valhalla.AddressableUtilities.Editor
{
	public static class ObjectExtensions
	{
		public static void SetupAddressable(this Object obj, string address, string groupKey = null, params string[] labels)
		{
			if (string.IsNullOrEmpty(address))
				throw new Exception("Can not set an empty addressables ID.");
		
			var settings = AddressableAssetSettingsDefaultObject.Settings;
			var entry = obj.GetAddressableAssetEntry(settings);

			if (entry == null)
				entry = obj.MakeAnAddressable(address, groupKey, settings);
			else
				ChangeAddress(entry, address, settings);
			
			AddAddressableLabels(entry, settings, labels);
		}


		public static void AddAddressableLabels(this Object obj, params string[] labels)
		{
			var settings = AddressableAssetSettingsDefaultObject.Settings;
			var entry = obj.GetAddressableAssetEntry(settings);

			if (entry == null)
				throw new Exception($"Asset <{obj}> is not an addressable");

			AddAddressableLabels(entry, settings, labels);
		}
		
		
		private static void AddAddressableLabels(AddressableAssetEntry entry, AddressableAssetSettings settings, params string[] labels)
		{
			if (labels == null || labels.Length == 0)
				return;

			var definedLabels = settings.GetLabels();

			foreach (var label in labels)
			{
				if (!definedLabels.Contains(label))
				{
					settings.AddLabel(label);
					settings.SetDirty(AddressableAssetSettings.ModificationEvent.LabelAdded, label, true);
				}
				
				if (!entry.labels.Contains(label))
				{
					entry.labels.Add(label);
					settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryModified, entry, true);
				}
			}
		}


		private static void ChangeAddress(AddressableAssetEntry entry, string address, AddressableAssetSettings settings = null)
		{
			settings = GetSettingsSafe(settings);
			
			entry.address = address;
			
			settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryModified, entry, true);
		}


		public static AddressableAssetEntry MakeAnAddressable(this Object obj, string address, string groupKey = null, AddressableAssetSettings settings = null)
		{
			Debug.Log($"Creating addressable <{address}> in group <{groupKey}>");
			
			settings = GetSettingsSafe(settings);

			bool foundAsset = AssetDatabase.TryGetGUIDAndLocalFileIdentifier(obj, out var guid, out long _);
			if (!foundAsset)
				throw new NullReferenceException();

			var group = FindOrCreateGroup(groupKey, settings);
			var entry = CreateNewEntry(guid, address, group, settings);
			entry.address = address;
			
			settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryAdded, entry, true);

			return entry;
		}


		private static AddressableAssetEntry CreateNewEntry(string guid, string address, AddressableAssetGroup group, AddressableAssetSettings settings)
		{
			var entry = settings.CreateOrMoveEntry(guid, group, readOnly: false, postEvent: false);
			if (entry == null)
				throw new NullReferenceException($"Can't create addressable entry for <{address}> in group <{group.Name}>");

			return entry;
		}


		private static AddressableAssetGroup FindOrCreateGroup(string groupName, AddressableAssetSettings settings)
		{
			if (string.IsNullOrEmpty(groupName))
				return settings.DefaultGroup;

			var foundGroup = settings.FindGroup(groupName);
			if (foundGroup != null)
				return foundGroup;
			
			return settings.CreateGroup(groupName, false, false, true, null, typeof(ContentUpdateGroupSchema), typeof(BundledAssetGroupSchema));
		}


		public static string GetAddressableID(this Object obj)
		{
			AddressableAssetEntry entry = obj.GetAddressableAssetEntry();
			return entry != null 
				? entry.address 
				: "";
		}
		
		
		public static bool IsAddressable(this Object obj)
			=> obj.GetAddressableAssetEntry() != null;


		public static AddressableAssetEntry GetAddressableAssetEntry(this Object obj, AddressableAssetSettings settings = null)
		{
			settings = GetSettingsSafe(settings);

			bool foundAsset = AssetDatabase.TryGetGUIDAndLocalFileIdentifier(obj, out var guid, out long _);
			if (!foundAsset)
				return null;
			
			var path = AssetDatabase.GUIDToAssetPath(guid);
			if (!path.ToLower().Contains("assets"))
				return null;

			return settings.FindAssetEntry(guid);
		}


		private static AddressableAssetSettings GetSettingsSafe(AddressableAssetSettings settings = null)
		{
			if (settings == null)
				settings = AddressableAssetSettingsDefaultObject.Settings;

			if (settings == null)
				throw new NullReferenceException();

			return settings;
		}
	}
}