## [1.6.1] – 2023-08-23

### Added
- Construction of asset references with address or asset link

### Changed
- `AssetReferenceGenerator.dll` updated to v1.0.1


## [1.6.0] – 2023-08-23

### Added
- `[AddressableAsset]` validation now provides fix for non-addressable assets, that generates address from name
- `[AddressableAsset]` stores regexp and replace transformation scheme, so address can be generated from name by custom rules

### Changed
- `AssetReferenceAttributes.dll` updated to v1.0.1


## [1.5.2] – 2023-07-27

### Added
- Untyped address interface `IHaveAddress` for string-only contracts


## [1.5.1] – 2023-07-27

### Added
- Interface `IAddressableAssetReference` for third-party extensions


## [1.5.0] – 2023-07-27 

### Added
- Source generation of `AddressableAssetReference<T>` with `[AddressableAsset]` attribute
- Validation, that `[AddressableAsset]`-marked assets have an address

### Changed
- `KeyAssetReference` renamed to `AddressableAssetReference` for less confusion


## [1.4.2] – 2023-07-03

### Fixed
- Build links fix


## [1.4.1] – 2023-07-03

### Added
- `KeyAssetReference` validation


## [1.4.0] – 2023-07-03

### Added
- `ManagedAssetContext` – wrapper around `AssetMemoryManager`, that store pack of assets in single "context", and unload all of them at once

### Changed
- Licence year updated
- CI/CD updated for mudularity


## [1.3.3]

### Added
- It's possible to create `ManagedAssetCache` from string representation of addresses now


## [1.3.2]

### Fixed
- Checks for null added for IL2CPP support


## [1.3.1]

### Added
- Cancellation support


## [1.3.0]

### Added
- `ManagedAssetCache`, that encapsulates instance loading-unloading for a pack of assets


## [1.2.2]

### Fixed
- Validation now in sync with Key


## [1.2.1]

### Added
- Check, is asset an addressable

### Removed
- AssetBundle creation script


## [1.2.0]

### Added
- Asset management and reference counting
- Reference serialization with keys

### Changed
- Rebranded to "Valhalla"


## [1.1.2]

### Added
- It's possible to set bundle from code now


## [1.1.1]

### Fixed
- Setting value of asset to null only after executing callback


## [1.1.0]

### Added
- Interface `IManagedAsset` used instead of just class
- `AssetManager`, that helps to manage instances of `IManagedAsset`
- For editor edge-cases `PseudoManagedLegacyAsset` added. It's just old plain ref that implements `IManagedAsset` 

### Changed
- Changelog formatting slightly changed


## [1.0.0]

### Added
- `ManagedAsset` addressable UniTask wrapper for stateful reference usage
- `PrefabAssetReference` added for prefab references
- Extensions to avoid boilerplate code around addressables
