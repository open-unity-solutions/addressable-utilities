### About

Addressables-related day-to-day tools.

### Content of package

1. Extension methods to work with `GameObject`s like with addressables.
2. `ManagedAsset` - async wrapper around `AssetReferences`.
3. `AssetManager` – static holder of `ManagedAsset`, so you can safely work with same `ManagedAsset` from different places.
4. `PrefabAssetReference`, so you can select addressables references of prefabs. Type safe!


### Dependencies:
- `Addressables`
- `UniTask`
- `Odin Inspector`


### TODO:
- [ ] Add instantiation and instance counter to `AssetManager`
- [ ] Make Odin integration optional
- [ ] Make inspector editor for `ManagedAsset`
- [ ] Rewrite readme with examples
- [ ] Double check dependencies
- [ ] Write about `AssetManager`
