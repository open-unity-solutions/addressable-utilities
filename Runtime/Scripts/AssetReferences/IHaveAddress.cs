namespace AssetReferences
{
	public interface IHaveAddress
	{
		string Address { get; }
	}
}
