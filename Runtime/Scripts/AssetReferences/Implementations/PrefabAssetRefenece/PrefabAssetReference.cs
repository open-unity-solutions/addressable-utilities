using System;
using Valhalla.AddressableUtilities.AssetReferences;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.Util;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Valhalla.AddressableUtilities
{
	[Serializable]
	public abstract class PrefabAssetReference<TComponent> : AddressableAssetReference<TComponent>
		where TComponent : MonoBehaviour
	{
		private ResourceManager _resourceManager = null;
		
		
		public ResourceManager ResourceManager
		{
			get
			{
				if (_resourceManager == null)
					_resourceManager = new ResourceManager(new DefaultAllocationStrategy());
				return _resourceManager;
			}
		}
		
		
		protected PrefabAssetReference(string guid) : base(guid)
		{
		}


		public override AsyncOperationHandle<TComponent> LoadAssetAsync()
		{
			Debug.Log($"Loading prefab for {typeof(TComponent)}");
			
			// TODO factory pattern
			AsyncOperationHandle<GameObject> objectHandler = LoadAssetAsync<GameObject>();
			LoadPrefabComponentOperation<TComponent> getComponentOperation = new LoadPrefabComponentOperation<TComponent>(objectHandler);
			
			return ResourceManager.StartOperation(getComponentOperation, objectHandler);
		}
	}
}
