using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;


namespace Valhalla.AddressableUtilities
{
	public class LoadPrefabComponentOperation<TComponent> : AsyncOperationBase<TComponent>
		where TComponent : MonoBehaviour
	{
		
		private readonly AsyncOperationHandle<GameObject> _dependencyLoadGameObjectHandle;

		
		public LoadPrefabComponentOperation(AsyncOperationHandle<GameObject> dependencyLoadGameObjectHandle) : base()
		{
			_dependencyLoadGameObjectHandle = dependencyLoadGameObjectHandle;
		}
		
		
		protected override void Execute()
		{
			if (_dependencyLoadGameObjectHandle.Status == AsyncOperationStatus.Failed)
				Complete(null, false, _dependencyLoadGameObjectHandle.OperationException);
			else
				ExecuteAfterSuccessfulLoad();
		}


		private void ExecuteAfterSuccessfulLoad()
		{
			var gameObject = _dependencyLoadGameObjectHandle.Result;
			Result = gameObject.GetComponent<TComponent>();

			if (Result == null)
				Complete(Result, false, "Component not found");
			else
				Complete(Result, true, null);
		}
	}
}
