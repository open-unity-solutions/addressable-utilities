using System;
using AssetReferences;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using Valhalla.AddressableUtilities.Editor;
using UnityEditor;
#endif

namespace Valhalla.AddressableUtilities.AssetReferences
{
	[Serializable]
	public class AddressableAssetReference<TObject> : AssetReferenceT<TObject>, IAddressableAssetReference<Object>, ISelfValidator
		where TObject : UnityEngine.Object
	{
		[FormerlySerializedAs("_key")]
		[SerializeField]
		private string _address = null;


		public string Address
			=> _address;


		public override object RuntimeKey
		{
			get
			{
				if (string.IsNullOrEmpty(_address))
				{
					Debug.LogError("Addressable reference path is not serialized");

					return "";
				}
				
				return Address;
			}
		}


		protected AddressableAssetReference(string guid) : base(guid)
		{
			
		}
		
		
		protected AddressableAssetReference(string guid, string address) : this(guid)
		{
			_address = address;
		}
		
		#if UNITY_EDITOR
		
		protected AddressableAssetReference(TObject asset) : base(AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(asset)))
		{
			if (!asset.IsAddressable())
				throw new NullReferenceException($"Asset {asset} must be addressable");
			
			_address = asset.GetAddressableID();
		}

		#endif
		
		public override bool ValidateAsset(string mainAssetPath)
		{
			#if UNITY_EDITOR

			var component = AssetDatabase.LoadAssetAtPath<TObject>(mainAssetPath);

			if (component != null)
				return true;
			
			#endif
			
			return false;
		}


		public override bool RuntimeKeyIsValid()
			=> !string.IsNullOrEmpty(_address);


		#if UNITY_EDITOR


		public override bool SetEditorAsset(Object value)
		{
			bool isCorrect = base.SetEditorAsset(value);

			if (!isCorrect)
			{
				_address = null;
				return isCorrect;
			}

			if (value == null)
			{
				_address = null;
			}
			else
				_address = value.GetAddressableID();
			Debug.Log(_address);

			
			return isCorrect;
		}
		
		#endif
		
		
		public void Validate(SelfValidationResult result)
		{
			#if UNITY_EDITOR
			if (editorAsset == null)
			{
				result.AddWarning("Editor asset is null");
				return;
			}
			
			var assetAddress = editorAsset.GetAddressableID();

			if (string.IsNullOrEmpty(assetAddress))
			{
				result.AddError("Referenced asset is not an addressable");
				return;
			}

			if (assetAddress != Address)
			{
				result.AddError($"Incorrect key cached: key <{Address}> != address <{assetAddress}>");
				return;
			}
			#endif
		}
	}
}
