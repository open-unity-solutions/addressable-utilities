namespace AssetReferences
{
	public interface IAddressableAssetReference<TAsset> : IHaveAddress
		where TAsset : UnityEngine.Object
	{
		
	}
}
