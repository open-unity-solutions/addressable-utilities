using System;
using UnityEngine;
using UnityEngine.AddressableAssets;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Valhalla.AddressableUtilities.Runtime.Extensions
{
	public static class AssetReferenceExtensions
	{
		public static string GetAddressableKey(this AssetReference reference)
		{
			return reference.RuntimeKey.ToString();
		}
	}
}
