using System;
using System.Threading;
using AssetReferences;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;


namespace Valhalla.AddressableUtilities
{
	[Serializable]
	public abstract class BaseManagedAsset<TAsset> : IManagedAsset<TAsset>
		where TAsset : UnityEngine.Object
	{
		public abstract UniTask<TAsset> LoadAssetAsync(CancellationToken cancellationToken = default);


		public abstract void ReleaseAsset();


		public abstract void Dispose();
		
		[ShowInInspector]
		protected readonly string _address;
		
		
		[ShowInInspector]
		protected TAsset _asset;


		[ShowInInspector]
		protected bool _isLoaded;
		
		
		public bool IsLoaded
			=> _isLoaded;


		public string Address
			=> _address;
		
		
		public TAsset Asset
		{
			get
			{
				if (!_isLoaded)
					throw new Exception($"Referenced addressable asset <{_address}> is not loaded");
					
				return _asset;
			}
		}
		
		
		protected BaseManagedAsset(TAsset value, bool isLoaded, string address)
		{
			_asset = value;
			_isLoaded = isLoaded;
			_address = address;
		}
		
	}
}
