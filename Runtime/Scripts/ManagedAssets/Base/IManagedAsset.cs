using System;
using System.Threading;
using AssetReferences;
using Cysharp.Threading.Tasks;


namespace Valhalla.AddressableUtilities
{
	public interface IManagedAsset<TAsset> : IAddressableAssetReference<TAsset>, IDisposable
		where TAsset : UnityEngine.Object
	{
		bool IsLoaded { get; }
		TAsset Asset { get; }
		
		UniTask<TAsset> LoadAssetAsync(CancellationToken cancellationToken = default);
		void ReleaseAsset();
	}
}
