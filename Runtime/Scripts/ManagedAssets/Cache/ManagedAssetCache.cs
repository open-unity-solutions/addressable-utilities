using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.ResourceManagement.Exceptions;
using Valhalla.AddressableUtilities.AssetReferences;
using Valhalla.Serialization;


namespace Valhalla.AddressableUtilities.MemoryManagement
{
	[Serializable]
	public class ManagedAssetCache<TAsset> : IDisposable
		where TAsset : UnityEngine.Object
	{
		private readonly AssetMemoryManager<TAsset> _relatedManager;
		
		
		[ShowInInspector, ReadOnly]
		private readonly IList<string> _keys;
		
		
		[ShowInInspector, ReadOnly]
		private readonly UnityDictionary<string, TAsset> _loadedAssets = new();
		
		private readonly UnityDictionary<string, Guid> _releaseTokens = new();
		
		
		[ShowInInspector, ReadOnly]
		public bool IsLoaded { get; private set; }
		
		[ShowInInspector, ReadOnly]
		public bool IsLoading { get; private set; }
		
		[ShowInInspector, ReadOnly]
		private bool _isDisposed;


		private CancellationTokenSource _disposeCts = new();
		

		
		public static ManagedAssetCache<TAsset> Create<TAddressableReference>(AssetMemoryManager<TAsset> relatedManager, IList<TAddressableReference> references)
			where TAddressableReference : AddressableAssetReference<TAsset>
			=> new (relatedManager, references.Select(reference => reference.Address).ToList());
		
		
		public static ManagedAssetCache<TAsset> Create(AssetMemoryManager<TAsset> relatedManager, IList<AddressableAssetReference<TAsset>> references)
			=> new (relatedManager, references.Select(reference => reference.Address).ToList());
		
		
		public static ManagedAssetCache<TAsset> Create(AssetMemoryManager<TAsset> relatedManager, IList<string> keys)
			=> new (relatedManager, keys);


		private ManagedAssetCache(AssetMemoryManager<TAsset> relatedManager, IList<string> keys)
		{
			_relatedManager = relatedManager;
			_keys = keys;
		}
		
		
		~ManagedAssetCache()
		{
			Dispose();
		}


		public void Dispose()
		{
			if (_isDisposed)
				return;

			_isDisposed = true;

			if (IsLoading && _disposeCts != null)
				_disposeCts.Cancel();
			
			if (_loadedAssets != null)
				_loadedAssets.Clear();
			
			if (_releaseTokens != null)
			{
				if (_relatedManager != null)
					foreach (var (key, token) in _releaseTokens)
						_relatedManager.ReleaseAsset(key, token);

				_releaseTokens.Clear();
			}
		}

		
		public async UniTask<IReadOnlyDictionary<string, TAsset>> LoadAllAsync(CancellationToken cancellationToken = default)
		{
			if (_isDisposed)
				throw new ObjectDisposedException(nameof(ManagedAssetCache<TAsset>));
			
			if (IsLoaded)
			{
				Debug.LogError("Already loaded. Are you sure you need to call this twice?");
				return GetAssets();
			}
			
			if (IsLoading)
				throw new OperationException("Loading is already in progress");
			
			
			IsLoading = true;

			var cts = CancellationTokenSource.CreateLinkedTokenSource(_disposeCts.Token, cancellationToken);
			
			await ProcessLoadingAsync(cts.Token).SuppressCancellationThrow();

			if (cancellationToken.IsCancellationRequested)
			{
				IsLoading = false;

				Dispose();
				
				throw new OperationCanceledException(cancellationToken);
			}
			
			IsLoading = false;
			IsLoaded = true;

			return GetAssets();
		}


		public IReadOnlyDictionary<string, TAsset> GetAssets()
		{
			if (!IsLoaded)
				throw new InvalidOperationException("Cache is not loaded yet");
			
			if (_isDisposed)
				throw new ObjectDisposedException(nameof(ManagedAssetCache<TAsset>));
			
			return _loadedAssets;
		}


		public TAsset GetAsset(AddressableAssetReference<TAsset> assetReference)
			=> GetAsset(assetReference.Address);


		public TAsset GetAsset(string key)
			=> GetAssets()[key];
		
		

		private async UniTask ProcessLoadingAsync(CancellationToken cancellationToken = default)
		{
			var loadingTasks = new List<UniTask>();
			foreach (var key in _keys)
			{
				var task = LoadSingleAssetAsync(key, cancellationToken);
				loadingTasks.Add(task);
			}
			await UniTask.WhenAll(loadingTasks);
		}


		private async UniTask LoadSingleAssetAsync(string key, CancellationToken cancellationToken = default)
		{
			var (asset, releaseToken) = await _relatedManager.LoadAssetAsync(key, cancellationToken);

			if (_isDisposed)
			{
				_relatedManager.ReleaseAsset(key, releaseToken);
				return;
			}
			
			_loadedAssets.Add(key, asset);
			_releaseTokens.Add(key, releaseToken);
		}
		
	}
}
