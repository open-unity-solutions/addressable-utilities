using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;


namespace Valhalla.AddressableUtilities
{
	[Serializable]
	public class ManagedAsset<TAsset> : BaseManagedAsset<TAsset>
		where TAsset : UnityEngine.Object
	{

		protected readonly Action _releaseAction;
		
		protected readonly Func<CancellationToken, UniTask<TAsset>> _loadAsyncAction;
		

		public ManagedAsset(AssetReferenceT<TAsset> reference)
			: base(null, false, reference.RuntimeKey.ToString())
		{
			_loadAsyncAction = token => reference.LoadAssetAsync().ToUniTask(cancellationToken: token);
			_releaseAction = reference.ReleaseAsset;
		}


		public ManagedAsset(string address)
			:base(null, false, address)
		{
			_loadAsyncAction = token => Addressables.LoadAssetAsync<TAsset>(address).ToUniTask(cancellationToken: token);
			_releaseAction = () => Addressables.Release(_asset);
		}


		public override async UniTask<TAsset> LoadAssetAsync(CancellationToken cancellationToken = default)
		{
			if (_isLoaded)
				throw new Exception($"Can't load addressable asset <{_address}>, it's already loaded");
			
			_asset = await _loadAsyncAction.Invoke(cancellationToken);
			_isLoaded = true;

			return _asset;
		}
		
		
		public override void ReleaseAsset()
		{
			if (!_isLoaded)
				throw new Exception($"Can't release addressable asset <{_address}>, it's already not loaded");
			
			_releaseAction.Invoke();
			_asset = null;
			_isLoaded = false;
		}

		
		public override void Dispose()
			=> ReleaseAsset();
	}
}
