using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine.AddressableAssets;
using Valhalla.Serialization;


namespace Valhalla.AddressableUtilities.MemoryManagement
{
	[Serializable]
	public class AssetMemoryManager<TAsset>
		where TAsset : UnityEngine.Object
	{
		[ShowInInspector, ReadOnly]
		private UnityDictionary<string, AssetMemoryState<TAsset>> _assets = new();


		public TAsset GetLoadedAsset(AssetReferenceT<TAsset> reference, Guid releaseToken)
		{
			var key = reference.RuntimeKey.ToString();

			return GetLoadedAsset(key, releaseToken);
		}
		
		
		public TAsset GetLoadedAsset(string assetKey, Guid releaseToken)
		{
			if (!_assets.ContainsKey(assetKey))
				throw new NullReferenceException($"Asset <{assetKey}> never loaded");
			
			return _assets[assetKey].GetLoadedAsset(releaseToken);
		}
		
		
		public List<string> GetLoadedAssetsKeys()
		{
			return _assets
				.Where(asset => asset.Value.LoadedReferenceCount > 0)
				.Select(asset => asset.Key)
				.ToList();
		}


		public async UniTask<(TAsset asset, Guid releaseToken)> LoadAssetAsync(AssetReferenceT<TAsset> reference, CancellationToken cancellationToken = default)
		{
			var key = reference.RuntimeKey.ToString();
			
			if (_assets.ContainsKey(key))
				return await _assets[key].LoadAssetAsync(cancellationToken);

			return await LoadNewAssetAsync(key);
		}


		public async UniTask<(TAsset asset, Guid releaseToken)> LoadAssetAsync(string key, CancellationToken cancellationToken = default)
		{
			if (_assets.ContainsKey(key))
				return await _assets[key].LoadAssetAsync(cancellationToken);

			return await LoadNewAssetAsync(key);
		}
		
		
		private async UniTask<(TAsset asset, Guid releaseToken)> LoadNewAssetAsync(string key, CancellationToken cancellationToken = default)
		{
			var managedAsset = new AssetMemoryState<TAsset>(key);
			
			var result = await managedAsset.LoadAssetAsync(cancellationToken);
			_assets.Add(key, managedAsset);
			
			return result;
		}
		
		
		public void ReleaseAsset(string key, Guid releaseToken)
		{
			if (!_assets.ContainsKey(key))
				throw new Exception($"Asset <{key}> not found in cache (for token <{releaseToken}>)");

			_assets[key].ReleaseAsset(releaseToken);
		}
		
		
		public void ReleaseAsset(AssetReferenceT<TAsset> reference, Guid releaseToken)
		{
			var key = reference.RuntimeKey.ToString();

			ReleaseAsset(key, releaseToken);
		}


	}
}
