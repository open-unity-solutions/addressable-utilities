using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Valhalla.AddressableUtilities.Runtime.Extensions;
using Valhalla.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Valhalla.AddressableUtilities.MemoryManagement
{
	[Serializable]
	public class AssetMemoryState<TAsset>
		where TAsset : UnityEngine.Object
	{
		
		[ShowInInspector, ReadOnly]
		private readonly string _key;
		
		
		[ShowInInspector, ReadOnly]
		private readonly UnityHashSet<Guid> _activeTokens = new();

		
		private ManagedAsset<TAsset> _managedAsset;


		[ShowInInspector, ReadOnly]
		public int LoadedReferenceCount
			=> _activeTokens.Count;
		
		
		#if UNITY_EDITOR


		[PropertyOrder(-1)]
		[ShowInInspector, ReadOnly]
		private readonly string _assetPath;
		
		#endif


		public AssetMemoryState(string key)
		{
			_key = key;
		}
		
		
		public AssetMemoryState(AssetReferenceT<TAsset> reference)
		{
			_key = reference.GetAddressableKey();
			
			#if UNITY_EDITOR
			_assetPath = AssetDatabase.GetAssetPath(reference.editorAsset);
			#endif
		}
		
		
		public TAsset GetLoadedAsset(Guid releaseToken)
		{
			if (!_activeTokens.Contains(releaseToken))
				throw new ArgumentException($"Token <{releaseToken}> for asset <{_key}> is invalid, can't unload");

			return _managedAsset.Asset;
		}
		
		
		public async UniTask<(TAsset asset, Guid releaseToken)> LoadAssetAsync(CancellationToken cancellationToken = default)
		{
			if (_managedAsset == null)
				_managedAsset = new ManagedAsset<TAsset>(_key);

			if (!_managedAsset.IsLoaded)
			{
				TAsset result;
				try
				{
					result = await _managedAsset.LoadAssetAsync(cancellationToken);
				}
				catch (Exception e)
				{
					Debug.LogError($"Can't load asset <{_key}> due error: {e.GetType().Name} | {e.Message}");

					throw;
				}
				
				if (result == null)
					throw new NullReferenceException($"Asset <{_key}> not found");
			}

			var releaseToken = Guid.NewGuid();
			_activeTokens.Add(releaseToken);
			
			return (_managedAsset.Asset, releaseToken);
		}
		
		
		public void ReleaseAsset(Guid releaseToken)
		{
			if (!_activeTokens.Contains(releaseToken))
			{
				Debug.LogError($"Token <{releaseToken}> for asset <{_key}> not found, can't unload");
				return;
			}

			_activeTokens.Remove(releaseToken);
			
			if (LoadedReferenceCount == 0)
				_managedAsset.ReleaseAsset();
		}
	}
}
