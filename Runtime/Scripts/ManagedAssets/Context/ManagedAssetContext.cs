using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;


namespace Valhalla.AddressableUtilities.MemoryManagement
{
	[Serializable]
	public class ManagedAssetContext<TAsset> : IDisposable
		where TAsset : UnityEngine.Object
	{
		private readonly AssetMemoryManager<TAsset> _assetMemoryManager;
		
		[ShowInInspector, ReadOnly]
		private readonly Dictionary<string, Guid> _loadedTokens = new();
		
		
		public bool IsClean
			=> _loadedTokens == null || _loadedTokens.Count == 0;
		
		
		public ManagedAssetContext(AssetMemoryManager<TAsset> assetMemoryManager)
		{
			_assetMemoryManager = assetMemoryManager;
		}


		~ManagedAssetContext()
		{
			ReleaseAllAssets();
		}
		
		
		public void Dispose()
		{
			ReleaseAllAssets();
		}
		
		
		public async UniTask<TAsset> ReadAssetAsync(string address)
		{
			if (_loadedTokens.TryGetValue(address, out var token))
				return _assetMemoryManager.GetLoadedAsset(address, token);

			var (loadedAsset, releaseToken) = await _assetMemoryManager.LoadAssetAsync(address);
			_loadedTokens[address] = releaseToken;
			return loadedAsset;
		}


		public bool IsLoadedInContext(string address)
			=> _loadedTokens.ContainsKey(address);


		public void ReleaseAllAssets()
		{
			if (IsClean)
				return;
			
			foreach (var (address, releaseToken) in _loadedTokens)
				_assetMemoryManager.ReleaseAsset(address, releaseToken);
			_loadedTokens.Clear();
		}

	}
}
